﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.3.93 SP?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="AxesCtrl" Source="Source.AxesHandling.AxesCtrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Robot2Ax" Source="Source.System.Robot2Axes.Robot2Ax.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="Robot3Ax" Source="Source.System.Robot3Axes.Robot3Ax.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="Delta2Ax" Source="Source.System.Delta2Axes.Delta2Ax.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="RobotRebel" Source="Source.System.ComauRebel.RobotRebel.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="Jog2AxRb" Source="Source.System.Robot2Axes.Jog2AxRb.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="Jog3AxRb" Source="Source.System.Robot3Axes.Jog3AxRb.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="JogDelta2A" Source="Source.System.Delta2Axes.JogDelta2Ax.prg" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <Task Name="JogRebelRb" Source="Source.System.ComauRebel.JogRebelRb.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4" />
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8">
    <Task Name="VisuCtrl" Source="Source.HMI.VisuCtrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <DataObjects>
    <DataObject Name="Acp10sys" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <NcDataObjects>
    <NcDataObject Name="acp10etxen" Source="Source.AxesHandling.MotionCfg.acp10etxen.dob" Memory="UserROM" Language="Ett" />
    <NcDataObject Name="gAxQ1a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ1obj.gAxQ1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ1i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ1obj.gAxQ1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxQ2a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ2obj.gAxQ2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ2i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ2obj.gAxQ2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxQ3a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ3obj.gAxQ3a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ3i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ3obj.gAxQ3i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxQ4a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ4obj.gAxQ4a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ4i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ4obj.gAxQ4i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxQ5a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ5obj.gAxQ5a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ5i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ5obj.gAxQ5i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxQ6a" Source="Source.AxesHandling.MotionCfg.Real.gAxQ6obj.gAxQ6a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxQ6i" Source="Source.AxesHandling.MotionCfg.Real.gAxQ6obj.gAxQ6i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxMasa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxMasobj.gVAxMasa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxMasi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxMasobj.gVAxMasi.dob" Memory="UserROM" Language="Vax" />
    <NcDataObject Name="gVAxXa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxXobj.gVAxXa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxXi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxXobj.gVAxXi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxYa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxYobj.gVAxYa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxYi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxYobj.gVAxYi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxZa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxZobj.gVAxZa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxZi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxZobj.gVAxZi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxAa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxAobj.gVAxAa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxAi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxAobj.gVAxAi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxBa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxBobj.gVAxBa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxBi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxBobj.gVAxBi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gVAxCa" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxCobj.gVAxCa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gVAxCi" Source="Source.AxesHandling.MotionCfg.Virtual.gVAxCobj.gVAxCi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="PtoP" Source="" Memory="UserROM" Language="Binary" />
  </NcDataObjects>
  <VcDataObjects>
    <VcDataObject Name="Visu" Source="Source.HMI.Visu.dob" Memory="UserROM" Language="Vc" WarningLevel="2" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arsvcreg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FWRules" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arial" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchtml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccslider" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialbd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcxml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccurl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpkat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfar00" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="SDCmap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10map" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="acp10cfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="VirtualCfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="RobotCfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Robo" Source="Libraries.Robo.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="Acp10sdc" Source="Libraries.Acp10sdc.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="MpAxis" Source="Libraries.MpAxis.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpBase" Source="Libraries.MpBase.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10_MC" Source="Libraries.Acp10_MC.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="Acp10sim" Source="Libraries.Acp10sim.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="MpCom" Source="Libraries.MpCom.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asbrstr" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="fileio" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="powerlnk" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asieccon" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="dataobj" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>