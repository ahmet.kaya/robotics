
PROGRAM _INIT
	
	RobotRebel.inMpAxisQ1								:= ADR(gMpAxisBasic[AX_Q1]);
	RobotRebel.inMpAxisQ2								:= ADR(gMpAxisBasic[AX_Q2]);
	RobotRebel.inMpAxisQ3								:= ADR(gMpAxisBasic[AX_Q3]);
	RobotRebel.inMpAxisQ4								:= ADR(gMpAxisBasic[AX_Q4]);
	RobotRebel.inMpAxisX								:= ADR(gMpAxisBasic[AX_X]);
	RobotRebel.inMpAxisY								:= ADR(gMpAxisBasic[AX_Y]);
	RobotRebel.inMpAxisZ								:= ADR(gMpAxisBasic[AX_Z]);
	RobotRebel.inMpAxisC								:= ADR(gMpAxisBasic[AX_C]);
	RobotRebel.inParameters.Home.Q1						:= 0.0;
	RobotRebel.inParameters.Home.Q2						:= 0.0;
	RobotRebel.inParameters.Home.Q3						:= 0.0;
	RobotRebel.inParameters.Home.Q4						:= 0.0;
	RobotRebel.inParameters.Variation					:= REBEL_S6_060;
	RobotRebel.inEnable									:= TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Get Commands *)
	RobotRebel.inPower									:= gPower;
	RobotRebel.inHome									:= gHome;
	RobotRebel.inErrorReset								:= gReset;
	
	
	(* FB Call *)
	RobotRebel();
	
	
	(* Reset Commands *)
	IF RobotRebel.outIsHomed THEN
		gHome											:= FALSE;
	END_IF;
	
	IF NOT RobotRebel.outError THEN
		gReset											:= FALSE;
	END_IF;
	
	
	(* Get actual cartesian position values. *)
	gPos[0]		:= RobotRebel.outPosX;
	gPos[1]		:= RobotRebel.outPosY;
	gPos[2]		:= RobotRebel.outPosZ;
	gPos[3]		:= RobotRebel.outPosC;
	
	
	(*********************************** Send Position Values to SceneViewer ***********************************)
	PVIVars[0]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q1].Position);
	PVIVars[1]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q2].Position);
	PVIVars[2]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q3].Position);
	PVIVars[3]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q4].Position);
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

