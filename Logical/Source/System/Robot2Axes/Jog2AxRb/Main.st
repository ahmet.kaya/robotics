
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM



PROGRAM _CYCLIC
	
	IF 	gMpAxisBasic[AX_X].PowerOn AND gMpAxisBasic[AX_X].IsHomed AND NOT gMpAxisBasic[AX_X].Error AND
		gMpAxisBasic[AX_Z].PowerOn AND gMpAxisBasic[AX_Z].IsHomed AND NOT gMpAxisBasic[AX_Z].Error THEN
		
		(* Parameters *)
		gMpAxisBasicPar[AX_Z].Jog.Velocity			:= gMpAxisBasicPar[AX_X].Jog.Velocity;
		gMpAxisBasicPar[AX_X].Jog.Acceleration		:= 5.0 * gMpAxisBasicPar[AX_X].Jog.Velocity;
		gMpAxisBasicPar[AX_X].Jog.Deceleration		:= gMpAxisBasicPar[AX_X].Jog.Acceleration;
		gMpAxisBasicPar[AX_Z].Jog.Acceleration		:= 5.0 * gMpAxisBasicPar[AX_Z].Jog.Velocity;
		gMpAxisBasicPar[AX_Z].Jog.Deceleration		:= gMpAxisBasicPar[AX_Z].Jog.Acceleration;
		gMpAxisBasicPar[AX_X].Jog.UpperLimit		:= 10000.0;
		gMpAxisBasicPar[AX_X].Jog.LowerLimit		:= - 10000.0;
		gMpAxisBasicPar[AX_Z].Jog.UpperLimit		:= 10000.0;
		gMpAxisBasicPar[AX_Z].Jog.LowerLimit		:= - 10000.0;
		
		(* Commands *)
		gMpAxisBasic[AX_X].JogPositive				:= gJog.JogPosX OR gJog.JogPosXPosZ OR gJog.JogPosXNegZ;
		gMpAxisBasic[AX_Z].JogPositive				:= gJog.JogPosZ OR gJog.JogPosXPosZ OR gJog.JogNegXPosZ;
		gMpAxisBasic[AX_X].JogNegative				:= gJog.JogNegX OR gJog.JogNegXPosZ OR gJog.JogNegXNegZ;
		gMpAxisBasic[AX_Z].JogNegative				:= gJog.JogNegZ OR gJog.JogPosXNegZ OR gJog.JogNegXNegZ;
		
	END_IF;
	 
END_PROGRAM



PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

