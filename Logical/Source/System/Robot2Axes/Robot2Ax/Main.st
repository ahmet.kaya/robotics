
PROGRAM _INIT
	
	Robot2Axes.inMpAxisQ1								:= ADR(gMpAxisBasic[AX_Q1]);
	Robot2Axes.inMpAxisQ2								:= ADR(gMpAxisBasic[AX_Q2]);
	Robot2Axes.inMpAxisX								:= ADR(gMpAxisBasic[AX_X]);
	Robot2Axes.inMpAxisZ								:= ADR(gMpAxisBasic[AX_Z]);
//	Robot2Axes.inParameters.Dimensions.Q1_to_Q2			:= FIRST_ARM;
//	Robot2Axes.inParameters.Dimensions.Q2_to_TCP		:= SECOND_ARM;
	Robot2Axes.inParameters.Home.Q1						:= 0.0;
	Robot2Axes.inParameters.Home.Q2						:= 0.0;
	Robot2Axes.inEnable									:= TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Get Commands *)
	Robot2Axes.inPower									:= gPower;
	Robot2Axes.inHome									:= gHome;
	Robot2Axes.inErrorReset								:= gReset;
	
	
	(* FB Call *)
	Robot2Axes();
	
	
	(* Reset Commands *)
	IF Robot2Axes.outIsHomed THEN
		gHome											:= FALSE;
	END_IF;
	
	IF NOT Robot2Axes.outError THEN
		gReset											:= FALSE;
	END_IF;
	
	
	(* Get actual cartesian position values. *)
	gPos[0]		:= Robot2Axes.outPosX;
	gPos[2]		:= Robot2Axes.outPosZ;
	
	
	(*********************************** Send Position Values to SceneViewer ***********************************)
	PVIVars[0]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q1].Position);
	PVIVars[1]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q2].Position);	
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

