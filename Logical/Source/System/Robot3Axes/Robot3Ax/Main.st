
PROGRAM _INIT
	
	Robot3Axes.inMpAxisQ1								:= ADR(gMpAxisBasic[AX_Q1]);
	Robot3Axes.inMpAxisQ2								:= ADR(gMpAxisBasic[AX_Q2]);
	Robot3Axes.inMpAxisQ3								:= ADR(gMpAxisBasic[AX_Q3]);
	Robot3Axes.inMpAxisX								:= ADR(gMpAxisBasic[AX_X]);
	Robot3Axes.inMpAxisY								:= ADR(gMpAxisBasic[AX_Y]);
	Robot3Axes.inMpAxisZ								:= ADR(gMpAxisBasic[AX_Z]);
	Robot3Axes.inParameters.Home.Q1						:= 0.0;
	Robot3Axes.inParameters.Home.Q2						:= 0.0;
	Robot3Axes.inParameters.Home.Q3						:= 0.0;
	Robot3Axes.inEnable									:= TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Get Commands *)
	Robot3Axes.inPower									:= gPower;
	Robot3Axes.inHome									:= gHome;
	Robot3Axes.inErrorReset								:= gReset;
	
	
	(* FB Call *)
	Robot3Axes();
	
	
	(* Reset Commands *)
	IF Robot3Axes.outIsHomed THEN
		gHome											:= FALSE;
	END_IF;
	
	IF NOT Robot3Axes.outError THEN
		gReset											:= FALSE;
	END_IF;
	
	
	(* Get actual cartesian position values. *)
	gPos[0]		:= Robot3Axes.outPosX;
	gPos[1]		:= Robot3Axes.outPosY;
	gPos[2]		:= Robot3Axes.outPosZ;
	
	
	(*********************************** Send Position Values to SceneViewer ***********************************)
	PVIVars[0]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q1].Position);
	PVIVars[1]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q2].Position);
	PVIVars[2]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q3].Position);
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

