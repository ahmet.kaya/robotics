
PROGRAM _INIT
	
	Delta2Axes.inMpAxisQ1								:= ADR(gMpAxisBasic[AX_Q1]);
	Delta2Axes.inMpAxisQ2								:= ADR(gMpAxisBasic[AX_Q2]);
	Delta2Axes.inMpAxisX								:= ADR(gMpAxisBasic[AX_X]);
	Delta2Axes.inMpAxisZ								:= ADR(gMpAxisBasic[AX_Z]);
	Delta2Axes.inParameters.Home.Q1						:= 0.0;
	Delta2Axes.inParameters.Home.Q2						:= 0.0;
	Delta2Axes.inParameters.Dimensions.BaseRadius		:= 400.0;	// mm
	Delta2Axes.inParameters.Dimensions.PlatformRadius	:= 200.0;	// mm
	Delta2Axes.inParameters.Dimensions.ArmLength		:= 700.0;	// mm
	Delta2Axes.inParameters.Dimensions.PipeLength		:= 1400.0;	// mm
	Delta2Axes.inParameters.Dimensions.ToolHolderLength	:= 300.0;	// mm
	Delta2Axes.inEnable									:= TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Get Commands *)
	Delta2Axes.inPower									:= gPower;
	Delta2Axes.inHome									:= gHome;
	Delta2Axes.inErrorReset								:= gReset;
	
	
	(* FB Call *)
	Delta2Axes();
	
	
	(* Reset Commands *)
	IF Delta2Axes.outIsHomed THEN
		gHome											:= FALSE;
	END_IF;
	
	IF NOT Delta2Axes.outError THEN
		gReset											:= FALSE;
	END_IF;
	
	
	(* Get actual cartesian position values. *)
	gPos[0]		:= Delta2Axes.outPosX;
	gPos[2]		:= Delta2Axes.outPosZ;
	
	
	(*********************************** Send Position Values to SceneViewer ***********************************)
	PVIVars[0]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q1].Position);
	PVIVars[1]						:= LREAL_TO_REAL(gMpAxisBasic[AX_Q2].Position);
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

