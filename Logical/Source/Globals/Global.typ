(*Permanent Types*)

TYPE
	PermanentType : 	STRUCT 
		Duration : REAL;
		PickTime : REAL;
		ReleaseTime : REAL;
		ParkPos : PermanentPosType;
		PickPos : PermanentPosType;
		ReleasePos : PermanentPosType;
		Heigth : ARRAY[0..3]OF REAL;
	END_STRUCT;
	PermanentPosType : 	STRUCT 
		X : LREAL;
		Z : LREAL;
	END_STRUCT;
END_TYPE

(**)
(*Jog Types*)

TYPE
	JogType : 	STRUCT 
		JogPosX : BOOL;
		JogNegX : BOOL;
		JogPosY : BOOL;
		JogNegY : BOOL;
		JogPosZ : BOOL;
		JogNegZ : BOOL;
		JogPosXPosZ : BOOL;
		JogPosXNegZ : BOOL;
		JogNegXPosZ : BOOL;
		JogNegXNegZ : BOOL;
	END_STRUCT;
END_TYPE
