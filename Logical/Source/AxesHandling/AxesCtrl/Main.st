(*********************************************************************************
 * Copyright: Bernecker+Rainer 
 * Author:    kayaa 
 * Created:   November 19, 2019/8:53 AM 
 *********************************************************************************)

PROGRAM _INIT
	
	(* Get Mplink references for corresponding axes. *)
	gMpLinkAdr[AX_V_MAS]			:= ADR(gAxisVAxMas);
	gMpLinkAdr[AX_Q1]				:= ADR(gAxisQ1);
	gMpLinkAdr[AX_Q2]				:= ADR(gAxisQ2);
	gMpLinkAdr[AX_Q3]				:= ADR(gAxisQ3);
	gMpLinkAdr[AX_Q4]				:= ADR(gAxisQ4);
	gMpLinkAdr[AX_Q5]				:= ADR(gAxisQ5);
	gMpLinkAdr[AX_Q6]				:= ADR(gAxisQ6);
	gMpLinkAdr[AX_X]				:= ADR(gAxisVAxX);
	gMpLinkAdr[AX_Y]				:= ADR(gAxisVAxY);
	gMpLinkAdr[AX_Z]				:= ADR(gAxisVAxZ);
	gMpLinkAdr[AX_A]				:= ADR(gAxisVAxA);
	gMpLinkAdr[AX_B]				:= ADR(gAxisVAxB);
	gMpLinkAdr[AX_C]				:= ADR(gAxisVAxC);
	
	(* Get ACP10 axis references for corresponding axes. *)
	gAxAdr[AX_V_MAS]				:= ADR(gVAxMas);
	gAxAdr[AX_Q1]					:= ADR(gAxQ1);
	gAxAdr[AX_Q2]					:= ADR(gAxQ2);
	gAxAdr[AX_Q3]					:= ADR(gAxQ3);
	gAxAdr[AX_Q4]					:= ADR(gAxQ4);
	gAxAdr[AX_Q5]					:= ADR(gAxQ5);
	gAxAdr[AX_Q6]					:= ADR(gAxQ6);
	gAxAdr[AX_X]					:= ADR(gVAxX);
	gAxAdr[AX_Y]					:= ADR(gVAxY);
	gAxAdr[AX_Z]					:= ADR(gVAxZ);
	gAxAdr[AX_A]					:= ADR(gVAxA);
	gAxAdr[AX_B]					:= ADR(gVAxB);
	gAxAdr[AX_C]					:= ADR(gVAxC);
	
	(* Initialize all MpAxisBasic FBs. *)
	FOR i := 0 TO NUM_OF_AXES_INDEX DO
		gMpAxisBasic[i].MpLink		:= gMpLinkAdr[i];
		gMpAxisBasic[i].Axis		:= gAxAdr[i];
		gMpAxisBasic[i].Parameters	:= ADR(gMpAxisBasicPar[i]);
		gMpAxisBasic[i].Enable		:= TRUE;
		gMpAxisBasic[i]();
	END_FOR;
	 
END_PROGRAM



PROGRAM _CYCLIC
	
	(* Call FBs. *)
	FOR i := 0 TO NUM_OF_AXES_INDEX DO
		gMpAxisBasic[i]();
		ResetCmds;
	END_FOR;
	 
END_PROGRAM



PROGRAM _EXIT
	
	(* Disable all FBs which use MpLink. *)
	FOR i := 0 TO NUM_OF_AXES_INDEX DO
		gMpAxisBasic[i].Enable		:= FALSE;	gMpAxisBasic[i]();
	END_FOR;
	 
END_PROGRAM

