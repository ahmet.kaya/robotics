﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.2.98?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Objects>
    <Object Type="File" Description="Init, cyclic, exit code">Main.st</Object>
    <Object Type="File" Description="Local data types" Private="true">Types.typ</Object>
    <Object Type="File" Description="Local variables" Private="true">Variables.var</Object>
    <Object Type="Package" Description="All actions related to the task">Actions</Object>
  </Objects>
</Program>