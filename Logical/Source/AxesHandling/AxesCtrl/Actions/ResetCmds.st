
ACTION ResetCmds: 
		
	(***************************** MpAxisBasic FB *****************************) 
	IF gMpAxisBasic[i].InPosition AND gMpAxisBasic[i].MoveAdditive THEN
		gMpAxisBasic[i].MoveAdditive				:= FALSE;
	END_IF; 
		
	IF gMpAxisBasic[i].InPosition AND gMpAxisBasic[i].MoveAbsolute THEN
		gMpAxisBasic[i].MoveAbsolute				:= FALSE;
	END_IF;
		
	IF gMpAxisBasic[i].UpdateDone AND gMpAxisBasic[i].Update THEN
		gMpAxisBasic[i].Update						:= FALSE;
	END_IF;
		
	IF NOT gMpAxisBasic[i].Error AND gMpAxisBasic[i].ErrorReset THEN
		gMpAxisBasic[i].ErrorReset					:= FALSE;
	END_IF;
		
	IF gMpAxisBasic[i].Info.PLCopenState = mpAXIS_ERRORSTOP AND gMpAxisBasic[i].Power THEN
		gMpAxisBasic[i].Power						:= FALSE;
	END_IF;
		
	IF gMpAxisBasic[i].Error OR gMpAxisBasic[i].Stopped THEN
		gMpAxisBasic[i].Stop						:= FALSE;
		gMpAxisBasic[i].MoveAdditive				:= FALSE;
		gMpAxisBasic[i].MoveAbsolute				:= FALSE;
		gMpAxisBasic[i].MoveVelocity				:= FALSE;
		gMpAxisBasic[i].Home						:= FALSE;
		gMpAxisBasic[i].Update						:= FALSE;
		gMpAxisBasic[i].JogPositive					:= FALSE;
		gMpAxisBasic[i].JogNegative					:= FALSE;
	END_IF;

END_ACTION
