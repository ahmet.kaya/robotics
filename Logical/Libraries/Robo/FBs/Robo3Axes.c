
#include "RoboHeader.h"


/* Get the set position data in cartesian coordinate system and move the robot by using cyclic position set functionality. */
void Robo3Axes(struct Robo3Axes* inst)
{
	
	//============================================================================
	//							INTERNAL VARs	
	//============================================================================	
	struct MpAxisBasic				*_pMpAxQ1;
	struct MpAxisBasic				*_pMpAxQ2;
	struct MpAxisBasic				*_pMpAxQ3;
	struct MpAxisBasic				*_pMpAxX;
	struct MpAxisBasic				*_pMpAxY;
	struct MpAxisBasic				*_pMpAxZ;
	
	MpAxisBasicParType				*_pMpAxParQ1;
	MpAxisBasicParType				*_pMpAxParQ2;
	MpAxisBasicParType				*_pMpAxParQ3;
	MpAxisBasicParType				*_pMpAxParX;
	MpAxisBasicParType				*_pMpAxParY;
	MpAxisBasicParType				*_pMpAxParZ;
	
	ACP10AXIS_typ					*_pAxQ1;
	ACP10AXIS_typ					*_pAxQ2;
	ACP10AXIS_typ					*_pAxQ3;
	ACP10AXIS_typ					*_pAxX;
	ACP10AXIS_typ					*_pAxY;
	ACP10AXIS_typ					*_pAxZ;
	
	struct MC_BR_MoveCyclicPosition	*_pSetCyclicPosQ1;
	struct MC_BR_MoveCyclicPosition	*_pSetCyclicPosQ2;
	struct MC_BR_MoveCyclicPosition	*_pSetCyclicPosQ3;
	//============================================================================
	
	
	
	
	//============================================================================
	//		Get MpAxisBasic FBs and parameters for all axes	
	//============================================================================	
	if ((inst->inMpAxisQ1 != 0) && (inst->inMpAxisQ2 != 0) && (inst->inMpAxisQ3 != 0) && (inst->inMpAxisX != 0) && (inst->inMpAxisY != 0) && (inst->inMpAxisZ != 0))
	{
		_pMpAxQ1 = (struct MpAxisBasic	*) inst->inMpAxisQ1;
		_pMpAxQ2 = (struct MpAxisBasic	*) inst->inMpAxisQ2;
		_pMpAxQ3 = (struct MpAxisBasic	*) inst->inMpAxisQ3;
		_pMpAxX  = (struct MpAxisBasic	*) inst->inMpAxisX;
		_pMpAxY  = (struct MpAxisBasic	*) inst->inMpAxisY;
		_pMpAxZ  = (struct MpAxisBasic	*) inst->inMpAxisZ;
	}
	
	if ((_pMpAxQ1->Parameters != 0) && (_pMpAxQ2->Parameters != 0) && (_pMpAxQ3->Parameters != 0) && (_pMpAxX->Parameters != 0) && (_pMpAxY->Parameters != 0) && (_pMpAxZ->Parameters != 0))
	{
		_pMpAxParQ1  = (MpAxisBasicParType	*) _pMpAxQ1->Parameters;
		_pMpAxParQ2  = (MpAxisBasicParType	*) _pMpAxQ2->Parameters;
		_pMpAxParQ3  = (MpAxisBasicParType	*) _pMpAxQ3->Parameters;
		_pMpAxParX   = (MpAxisBasicParType	*) _pMpAxX->Parameters;
		_pMpAxParY   = (MpAxisBasicParType	*) _pMpAxY->Parameters;
		_pMpAxParZ   = (MpAxisBasicParType	*) _pMpAxZ->Parameters;
	}
	
	_pAxQ1 = (ACP10AXIS_typ *) _pMpAxQ1->Axis;
	_pAxQ2 = (ACP10AXIS_typ *) _pMpAxQ2->Axis;
	_pAxQ3 = (ACP10AXIS_typ *) _pMpAxQ3->Axis;
	_pAxX  = (ACP10AXIS_typ *) _pMpAxX->Axis;
	_pAxY  = (ACP10AXIS_typ *) _pMpAxY->Axis;
	_pAxZ  = (ACP10AXIS_typ *) _pMpAxZ->Axis;
	
	_pSetCyclicPosQ1 = (MC_BR_MoveCyclicPosition_typ *) &inst->_Internal.FB.SetCyclicPosQ1;
	_pSetCyclicPosQ2 = (MC_BR_MoveCyclicPosition_typ *) &inst->_Internal.FB.SetCyclicPosQ2;
	_pSetCyclicPosQ3 = (MC_BR_MoveCyclicPosition_typ *) &inst->_Internal.FB.SetCyclicPosQ3;
	
	_pSetCyclicPosQ1->Axis = _pAxQ1;
	_pSetCyclicPosQ2->Axis = _pAxQ2;
	_pSetCyclicPosQ3->Axis = _pAxQ3;
	//============================================================================
	
	
	
	
	//============================================================================
	//						Error Handling	
	//============================================================================	
	if (_pMpAxQ1->Error || _pMpAxQ2->Error || _pMpAxQ3->Error || _pMpAxX->Error || _pMpAxY->Error || _pMpAxZ->Error || _pSetCyclicPosQ1->Error || _pSetCyclicPosQ2->Error || _pSetCyclicPosQ3->Error)
	{
		inst->outError = 1;
		
		/* Reset all commands */
		if (!inst->inErrorReset)
		{
			_pMpAxQ1->ErrorReset = 0;
			_pMpAxQ2->ErrorReset = 0;
			_pMpAxQ3->ErrorReset = 0;
			_pMpAxX->ErrorReset  = 0;
			_pMpAxY->ErrorReset  = 0;
			_pMpAxZ->ErrorReset  = 0;
			
			_pMpAxQ1->Home = 0;
			_pMpAxQ2->Home = 0;
			_pMpAxQ3->Home = 0;
			_pMpAxX->Home  = 0;
			_pMpAxY->Home  = 0;
			_pMpAxZ->Home  = 0;
			
			if (!_pMpAxQ1->PowerOn || !_pMpAxQ2->PowerOn || !_pMpAxQ3->PowerOn || !_pMpAxX->PowerOn || !_pMpAxY->PowerOn || !_pMpAxZ->PowerOn)
			{
				_pMpAxQ1->Power = 0;
				_pMpAxQ2->Power = 0;
				_pMpAxQ3->Power = 0;
				_pMpAxX->Power  = 0;
				_pMpAxY->Power  = 0;
				_pMpAxZ->Power  = 0;
			}
		}
		
		/* Put the FB in the error step. */
		inst->_Internal.Status.Step = ROBO_3_AXES_ERROR;
	}
	else
	{
		inst->outError = 0;
		
		_pMpAxQ1->ErrorReset = 0;
		_pMpAxQ2->ErrorReset = 0;
		_pMpAxQ3->ErrorReset = 0;
		_pMpAxX->ErrorReset  = 0;
		_pMpAxY->ErrorReset  = 0;
		_pMpAxZ->ErrorReset  = 0;
		
		if (inst->_Internal.Status.Step == ROBO_3_AXES_ERROR)
		{
			inst->_Internal.Status.Step = ROBO_3_AXES_WAIT;
		}
	}
	
	/* Get diagnostic information of each real and cartesian axes. */
	memcpy(&inst->outDiag.X, 	&_pMpAxX->Info.Diag, 	sizeof(inst->outDiag.X));
	memcpy(&inst->outDiag.Y, 	&_pMpAxY->Info.Diag, 	sizeof(inst->outDiag.Y));
	memcpy(&inst->outDiag.Z, 	&_pMpAxZ->Info.Diag, 	sizeof(inst->outDiag.Z));
	memcpy(&inst->outDiag.Q1, 	&_pMpAxQ1->Info.Diag, 	sizeof(inst->outDiag.Q1));
	memcpy(&inst->outDiag.Q2, 	&_pMpAxQ2->Info.Diag, 	sizeof(inst->outDiag.Q2));
	memcpy(&inst->outDiag.Q3, 	&_pMpAxQ3->Info.Diag, 	sizeof(inst->outDiag.Q3));
	//============================================================================
	
	
	
	
	//============================================================================
	//						Power Handling	
	//============================================================================	
	/* Power Off command */
	if (!inst->inPower && !inst->inStopSynch)
	{
		_pMpAxQ1->Power 		= 0;
		_pMpAxQ2->Power 		= 0;
		_pMpAxQ3->Power 		= 0;
		_pMpAxX->Power  		= 0;
		_pMpAxY->Power  		= 0;
		_pMpAxZ->Power  		= 0;
		_pSetCyclicPosQ1->Enable	= 0;
		_pSetCyclicPosQ2->Enable	= 0;
		_pSetCyclicPosQ3->Enable	= 0;
	}
	//============================================================================
	
	
	
	
	//============================================================================
	//					Get Power, Homing and InPosition Status	
	//============================================================================		
	inst->outPowerOn = _pMpAxQ1->PowerOn && _pMpAxQ2->PowerOn && _pMpAxQ3->PowerOn && _pMpAxX->PowerOn && _pMpAxY->PowerOn && _pMpAxZ->PowerOn;
	inst->outIsHomed = _pMpAxQ1->IsHomed && _pMpAxQ2->IsHomed && _pMpAxQ3->IsHomed && _pMpAxX->IsHomed && _pMpAxY->IsHomed && _pMpAxZ->IsHomed;
	inst->outInPosition = _pMpAxX->InPosition && _pMpAxY->InPosition && _pMpAxZ->InPosition;
	//============================================================================
	
	
	

	//============================================================================
	//						FB ENABLED OR DISABLED	
	//============================================================================
	if (inst->inEnable)
	{
		
		/*********************************** State Machine ***********************************/
		switch (inst->_Internal.Status.Step)
		{
			
			//============================================================================
			case ROBO_3_AXES_INIT:	// Wait for initialization.
				//============================================================================		
					
				inst->outActive 	= 0;
				_pMpAxQ1->Enable 	= 1;
				_pMpAxQ2->Enable 	= 1;
				_pMpAxQ3->Enable 	= 1;
				_pMpAxX->Enable 	= 1;
				_pMpAxY->Enable 	= 1;
				_pMpAxZ->Enable 	= 1;
				
				/* All axes initialized successfully. */
				if (_pMpAxQ1->Active && _pMpAxQ2->Active && _pMpAxQ3->Active && _pMpAxX->Active && _pMpAxY->Active && _pMpAxZ->Active)
				{
					inst->_Internal.Status.Step = ROBO_3_AXES_WAIT;
				}
				
				break;
			
			//============================================================================
			case ROBO_3_AXES_WAIT:	// Wait for any command.
				//============================================================================		
								
				inst->outActive = 1;
				
				/************************* Command Interface *************************/
				if (inst->inPower && !inst->outPowerOn)	/* Power On command */
				{
					inst->_Internal.Status.Step = ROBO_3_AXES_POWER;
				}
				else if (inst->inHome && !inst->_Internal.Status.HomeOld)	/* Home command */
				{
					inst->_Internal.Status.Step = ROBO_3_AXES_HOME_Q1_Q2_Q3;
				}
				else if (!inst->inHome && inst->_Internal.Status.HomeOld)	/* Reset old home command */
				{
					inst->_Internal.Status.HomeOld = 0;
				}
				else if (inst->inMoveAbsolute && (_pMpAxX->Info.PLCopenState == mpAXIS_STANDSTILL) && (_pMpAxY->Info.PLCopenState == mpAXIS_STANDSTILL) && (_pMpAxZ->Info.PLCopenState == mpAXIS_STANDSTILL) && !inst->_Internal.Status.MoveAbsoluteOld)	/* MoveAbsolute command */
				{
					/* Calculate the required velocity, acceleration and deceleration values for each two cartesian axes. */
					double delX 				= inst->inParameters.Position[0] - _pMpAxX->Position;
					double delY 				= inst->inParameters.Position[1] - _pMpAxY->Position;
					double delZ 				= inst->inParameters.Position[2] - _pMpAxZ->Position;
					double totalPos 			= sqrt(pow(delX,2.0) + pow(delY,2.0) + pow(delZ,2.0));
					float velocityX 			= (float) fabs(inst->inParameters.Velocity * (delX / totalPos));
					float velocityY 			= (float) fabs(inst->inParameters.Velocity * (delY / totalPos));
					float velocityZ 			= (float) fabs(inst->inParameters.Velocity * (delZ / totalPos));
					float accelerationX		 	= (float) fabs(inst->inParameters.Acceleration * (delX / totalPos));
					float accelerationY		 	= (float) fabs(inst->inParameters.Acceleration * (delY / totalPos));
					float accelerationZ		 	= (float) fabs(inst->inParameters.Acceleration * (delZ / totalPos));
					float decelerationX		 	= (float) fabs(inst->inParameters.Deceleration * (delX / totalPos));
					float decelerationY		 	= (float) fabs(inst->inParameters.Deceleration * (delY / totalPos));
					float decelerationZ		 	= (float) fabs(inst->inParameters.Deceleration * (delZ / totalPos));
				
					_pMpAxParX->Position 		= inst->inParameters.Position[0];
					_pMpAxParY->Position 		= inst->inParameters.Position[1];
					_pMpAxParZ->Position 		= inst->inParameters.Position[2];
					_pMpAxParX->Velocity 		= (velocityX > 0.0 ? velocityX : 10.0);
					_pMpAxParY->Velocity 		= (velocityY > 0.0 ? velocityY : 10.0);
					_pMpAxParZ->Velocity 		= (velocityZ > 0.0 ? velocityZ : 10.0);
					_pMpAxParX->Acceleration 	= (accelerationX > 0.0 ? accelerationX : 10.0);
					_pMpAxParY->Acceleration 	= (accelerationY > 0.0 ? accelerationY : 10.0);
					_pMpAxParZ->Acceleration 	= (accelerationZ > 0.0 ? accelerationZ : 10.0);
					_pMpAxParX->Deceleration 	= (decelerationX > 0.0 ? decelerationX : 10.0);
					_pMpAxParY->Deceleration 	= (decelerationY > 0.0 ? decelerationY : 10.0);
					_pMpAxParZ->Deceleration 	= (decelerationZ > 0.0 ? decelerationZ : 10.0);
				
					/* Set MoveAbsolute commands for each two cartesian axes. */
					_pMpAxX->MoveAbsolute = 1;
					_pMpAxY->MoveAbsolute = 1;
					_pMpAxZ->MoveAbsolute = 1;
					
					inst->_Internal.Status.Step = ROBO_3_AXES_MOVE_ABS;
				}
				else if (!inst->inMoveAbsolute && inst->_Internal.Status.MoveAbsoluteOld)	/* Reset old MoveAbsolute command */
				{
					inst->_Internal.Status.MoveAbsoluteOld = 0;
				}
				
				break;
			
			//============================================================================
			case ROBO_3_AXES_POWER:	// Switch on both two real axes.
				//============================================================================		
						
				//				_pMpAxQ1->Power = _pMpAxQ1->Info.ReadyToPowerOn;
				//				_pMpAxQ2->Power = _pMpAxQ2->Info.ReadyToPowerOn;
				//				_pMpAxQ3->Power = _pMpAxQ3->Info.ReadyToPowerOn;
				_pMpAxQ1->Power = 1;
				_pMpAxQ2->Power = 1;
				_pMpAxQ3->Power = 1;
				_pMpAxX->Power  = 1;
				_pMpAxY->Power  = 1;
				_pMpAxZ->Power  = 1;
				
				/* All axes were switched on successfully. */
				if (_pMpAxQ1->PowerOn && _pMpAxQ2->PowerOn && _pMpAxQ3->PowerOn && _pMpAxX->PowerOn && _pMpAxY->PowerOn && _pMpAxZ->PowerOn)
				{
					inst->_Internal.Status.Step = ROBO_3_AXES_WAIT;
				}
			
				break;
			
			//============================================================================
			case ROBO_3_AXES_HOME_Q1_Q2_Q3:	// Home both two real axes first.
				//============================================================================		
						
				if (inst->_Internal.Status.FirstInStep)
				{
					_pMpAxQ1->Home = 1;
					_pMpAxQ2->Home = 1;
					_pMpAxQ3->Home = 1;
					
					_pMpAxParQ1->Home.Position = inst->inParameters.Home.Q1;
					_pMpAxParQ2->Home.Position = inst->inParameters.Home.Q2;
					_pMpAxParQ3->Home.Position = inst->inParameters.Home.Q3;
					
					_pMpAxParQ1->Home.Mode = mpAXIS_HOME_MODE_ABSOLUTE;
					_pMpAxParQ2->Home.Mode = mpAXIS_HOME_MODE_ABSOLUTE;
					_pMpAxParQ3->Home.Mode = mpAXIS_HOME_MODE_ABSOLUTE;
				}
				else
				{
					/* All real axes were homed successfully. */
					if (_pMpAxQ1->IsHomed && _pMpAxQ2->IsHomed && _pMpAxQ3->IsHomed)
					{
						_pMpAxQ1->Home = 0;
						_pMpAxQ2->Home = 0;
						_pMpAxQ3->Home = 0;
						
						inst->_Internal.Status.HomeOld = 1;
						inst->_Internal.Status.Step = ROBO_3_AXES_HOME_X_Y_Z;
					}
				}		
				
				break;
			
			//============================================================================
			case ROBO_3_AXES_HOME_X_Y_Z:	// Home both two virtual axes last.
				//============================================================================		
						
				if (inst->_Internal.Status.FirstInStep)
				{
					/* Calculate the cartesian coordinates after real axes were home. Then, set it to virtual axes as home position. */
					double * actCartesianHomePos;
					double actRealHomePos[3];
					
					actRealHomePos[0] = _pMpAxQ1->Position;
					actRealHomePos[1] = _pMpAxQ2->Position;
					actRealHomePos[2] = _pMpAxQ3->Position;
					actCartesianHomePos = forwardTrfComauNJ165(actRealHomePos);
					
					_pMpAxX->Home = 1;
					_pMpAxY->Home = 1;
					_pMpAxZ->Home = 1;
					
					_pMpAxParX->Home.Position = *actCartesianHomePos;
					_pMpAxParY->Home.Position = *(actCartesianHomePos+1);
					_pMpAxParZ->Home.Position = *(actCartesianHomePos+2);
					
					_pMpAxParX->Home.Mode = mpAXIS_HOME_MODE_DIRECT;
					_pMpAxParY->Home.Mode = mpAXIS_HOME_MODE_DIRECT;
					_pMpAxParZ->Home.Mode = mpAXIS_HOME_MODE_DIRECT;
				}
				else
				{
					/* All axes were homed successfully. */
					if (_pMpAxX->IsHomed && _pMpAxY->IsHomed && _pMpAxZ->IsHomed)
					{
						_pMpAxX->Home = 0;
						_pMpAxY->Home = 0;
						_pMpAxZ->Home = 0;
						
						_pSetCyclicPosQ1->Enable = 1;
						_pSetCyclicPosQ2->Enable = 1;
						_pSetCyclicPosQ3->Enable = 1;
						
						inst->_Internal.Status.HomeOld = 1;
						inst->_Internal.Status.Step = ROBO_3_AXES_WAIT;
					}
				}		
				
				break;
				
			//============================================================================
			case ROBO_3_AXES_MOVE_ABS:	// Move to an absolute position in cartesian
				// coordinate system.
				//============================================================================		
					
				inst->_Internal.Status.MoveAbsoluteOld = 1;
				
				/* Movement is done or cancelled. */
				if ((_pMpAxX->InPosition && _pMpAxY->InPosition && _pMpAxZ->InPosition) || !inst->inMoveAbsolute)
				{
				_pMpAxX->MoveAbsolute = 0;
				_pMpAxY->MoveAbsolute = 0;
				_pMpAxZ->MoveAbsolute = 0;
						
				inst->_Internal.Status.Step = ROBO_3_AXES_WAIT;
				}
			
			//============================================================================
			case ROBO_3_AXES_ERROR:	// Error handling
				//============================================================================		
				
				if (inst->inErrorReset)
				{
					_pMpAxQ1->ErrorReset 	= 1;
					_pMpAxQ2->ErrorReset 	= 1;
					_pMpAxQ3->ErrorReset 	= 1;
					_pMpAxX->ErrorReset  	= 1;
					_pMpAxY->ErrorReset  	= 1;
					_pMpAxZ->ErrorReset  	= 1;
					_pSetCyclicPosQ1->Enable	= 0;
					_pSetCyclicPosQ2->Enable	= 0;
					_pSetCyclicPosQ3->Enable	= 0;
				}
				
				break;
		}
		/*************************************************************************************/
     
		
		
		
		//============================================================================
		//						Coupling Real Axes to SDC Axes	
		//============================================================================		
		if (inst->outPowerOn && inst->outIsHomed && !inst->outError)
		{
			double targetCartesianPos[3];
			double * targetRealPos;
			double * actCartesianPos;
			double actRealPos[3];
				
			/* Target real axis position calculations. */
			targetCartesianPos[0] = _pMpAxX->Position;
			targetCartesianPos[1] = _pMpAxY->Position;
			targetCartesianPos[2] = _pMpAxZ->Position;
				
			targetRealPos = inverseTrfComauNJ165(targetCartesianPos);
			
			/* Coupling for Q1 axis is activated here. */
			if (inst->inStopSynch)
			{
				_pSetCyclicPosQ1->Enable = 0;
			}
			_pSetCyclicPosQ1->CyclicPosition.Integer 	= floor(*targetRealPos);
			_pSetCyclicPosQ1->CyclicPosition.Real 		= *targetRealPos - _pSetCyclicPosQ1->CyclicPosition.Integer;
			
			/* Coupling for Q2 axis is activated here. */
			if (inst->inStopSynch)
			{
				_pSetCyclicPosQ2->Enable = 0;
			}
			_pSetCyclicPosQ2->CyclicPosition.Integer 	= floor(*(targetRealPos+1));
			_pSetCyclicPosQ2->CyclicPosition.Real 		= *(targetRealPos+1) - _pSetCyclicPosQ2->CyclicPosition.Integer;
			
			/* Coupling for Q3 axis is activated here. */
			if (inst->inStopSynch)
			{
				_pSetCyclicPosQ3->Enable = 0;
			}
			_pSetCyclicPosQ3->CyclicPosition.Integer 	= floor(*(targetRealPos+2));
			_pSetCyclicPosQ3->CyclicPosition.Real 		= *(targetRealPos+2) - _pSetCyclicPosQ3->CyclicPosition.Integer;
	
			/* Get the coupling status. */
			if (_pSetCyclicPosQ1->Valid && _pSetCyclicPosQ2->Valid && _pSetCyclicPosQ3->Valid)
			{
				inst->outSynchStopped = 0;
			}
			
			if (!_pSetCyclicPosQ1->Valid && !_pSetCyclicPosQ2->Valid && !_pSetCyclicPosQ3->Valid)
			{
				inst->outSynchStopped = 1;
			}
	
			/* Actual cartesian position calculations. */
			actRealPos[0] = _pMpAxQ1->Position;
			actRealPos[1] = _pMpAxQ2->Position;
			actRealPos[2] = _pMpAxQ3->Position;
			
			actCartesianPos = forwardTrfComauNJ165(actRealPos);
			
			inst->outPosX = *actCartesianPos;
			inst->outPosY = *(actCartesianPos+1);
			inst->outPosZ = *(actCartesianPos+2);
			
			/* Actual cartesian velocity calculation. */
			inst->outVelocity = sqrt(pow(_pMpAxX->Velocity,2.0) + pow(_pMpAxY->Velocity,2.0) + pow(_pMpAxZ->Velocity,2.0));
		}
		//============================================================================
		
     
	}
	else
	{
		
		inst->_Internal.Status.Step			= ROBO_3_AXES_INIT;
		
		/* Reset all outputs. */
		inst->outActive 					= _pMpAxQ1->Active || _pMpAxQ2->Active || _pMpAxQ3->Active || _pMpAxX->Active || _pMpAxY->Active || _pMpAxZ->Active;
		inst->outError 						= 0;
		inst->outVelocity					= 0.0;
		
		/* Reset all commands internally used. */
		_pMpAxQ1->Enable 					= 0;
		_pMpAxQ2->Enable 					= 0;
		_pMpAxQ3->Enable 					= 0;
		_pMpAxX->Enable 					= 0;
		_pMpAxY->Enable 					= 0;
		_pMpAxZ->Enable 					= 0;
		
		_pMpAxQ1->Power 					= 0;
		_pMpAxQ2->Power 					= 0;
		_pMpAxQ3->Power 					= 0;
		_pMpAxX->Power 						= 0;
		_pMpAxY->Power 						= 0;
		_pMpAxZ->Power 						= 0;
		
		_pMpAxQ1->Home 						= 0;
		_pMpAxQ2->Home 						= 0;
		_pMpAxQ3->Home 						= 0;
		_pMpAxX->Home 						= 0;
		_pMpAxY->Home 						= 0;
		_pMpAxZ->Home 						= 0;
		
		_pSetCyclicPosQ1->Enable			= 0;
		_pSetCyclicPosQ2->Enable			= 0;
		_pSetCyclicPosQ3->Enable			= 0;
		
	}
	//============================================================================	
	
	
	
	
	/************************* First in step check *************************/
	FIRST_IN_STEP(inst->_Internal.Status);
	
	
	
	
	//============================================================================
	//								FB CALLS	
	//============================================================================
	MC_BR_MoveCyclicPosition(&inst->_Internal.FB.SetCyclicPosQ1);
	MC_BR_MoveCyclicPosition(&inst->_Internal.FB.SetCyclicPosQ2);
	MC_BR_MoveCyclicPosition(&inst->_Internal.FB.SetCyclicPosQ3);
	
}




