#include "RoboHeader.h"

/* Convert the angle value in radian to degree. */
double rad2deg (double radValue)
{
	return radValue * 180.0 / M_PI;
}

/* Convert the angle value in degree to radian. */
double deg2rad (double degValue)
{
	return degValue / 180.0 * M_PI;
}