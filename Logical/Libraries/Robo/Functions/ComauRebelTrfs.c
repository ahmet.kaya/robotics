#include "RoboHeader.h"


/* Inverse transformation of the Comau Rebel robot for different variations is calculated in this function. */
double * inverseTrfComauRebel (double targetTCPPos[], double actJointPos[], int type)
{
	
	/*************** Variables **************/
	double targetJointPos[4];		// degree or mm
	double L2 = 275.0;				// mm
	double L1 = type*10.0 - L2;		// mm
	double q2Acos;
	
	
	/*************** Calculations **************/
	
	/***** Q3 *****/
	targetJointPos[2] = targetTCPPos[2] - 120.0;
	
	/* Check Limits */
	if (targetJointPos[2] > _COMAU_REBEL_Q3_POS_LIMIT)
	{
		targetJointPos[2] = _COMAU_REBEL_Q3_POS_LIMIT;
	}
	
	if (targetJointPos[2] < _COMAU_REBEL_Q3_NEG_LIMIT)
	{
		targetJointPos[2] = _COMAU_REBEL_Q3_NEG_LIMIT;
	}
	
	/***** Q2 *****/
	q2Acos = (pow(targetTCPPos[0],2.0) + pow(targetTCPPos[1],2.0) - pow(L1,2.0) - pow(L2,2.0)) / (2.0 * L1 * L2);
	
	if (q2Acos >= 1.0)
	{
		targetJointPos[1] = 0.0;
	}
	else if (-1.0 < q2Acos && q2Acos < 1)
	{
		targetJointPos[1] = acos(q2Acos);
		targetJointPos[1] = rad2deg(targetJointPos[1]);
		
		/* Check Limits */
		if (targetJointPos[1] > _COMAU_REBEL_Q2_POS_LIMIT)
		{
			targetJointPos[1] = _COMAU_REBEL_Q2_POS_LIMIT;
		}
	
		if (targetJointPos[1] < _COMAU_REBEL_Q2_NEG_LIMIT)
		{
			targetJointPos[1] = _COMAU_REBEL_Q2_NEG_LIMIT;
		}
		
		/* Find the solution that is close to the actual joint position. */
		targetJointPos[1] = (actJointPos[1] < 0.0) ? -targetJointPos[1] : targetJointPos[1];
	}
	else if (q2Acos <= -1.0)
	{
		if (abs(_COMAU_REBEL_Q2_POS_LIMIT - actJointPos[1]) < abs(_COMAU_REBEL_Q2_NEG_LIMIT - actJointPos[1]))
		{
	  		targetJointPos[1] = _COMAU_REBEL_Q2_POS_LIMIT;
		}
		else
		{
			targetJointPos[1] = _COMAU_REBEL_Q2_NEG_LIMIT;
		}
	}
	
	/***** Q1 *****/
	targetJointPos[0] = atan2(targetTCPPos[1], targetTCPPos[0]) - atan2((L2 * sin(deg2rad(targetJointPos[1]))), (L1 + L2 * cos(deg2rad(targetJointPos[1]))));
	targetJointPos[0] = rad2deg(targetJointPos[0]);
	
	/* Check Limits */
	if (targetJointPos[0] > _COMAU_REBEL_Q1_POS_LIMIT)
	{
		targetJointPos[0] = _COMAU_REBEL_Q1_POS_LIMIT;
	}
	
	if (targetJointPos[0] < _COMAU_REBEL_Q1_NEG_LIMIT)
	{
		targetJointPos[0] = _COMAU_REBEL_Q1_NEG_LIMIT;
	}
	
	/***** Q4 *****/
	targetJointPos[3] = - targetJointPos[0] - targetJointPos[1] + targetTCPPos[3];
	
		
	/*************** Return **************/
	return targetJointPos;
	
}



/* Forward transform of the Comau Rebel robot for different variations is calculated in this function to find the actual position. */
double * forwardTrfComauRebel (double actJointPos[], int type)
{
	
	/*************** Variables **************/
	double actualTCPPos[4];			// mm
	double L2 = 275.0;				// mm
	double L1 = type*10.0 - L2;		// mm
	
	
	/*************** Calculations **************/
	actualTCPPos[0] = L1 * cos(deg2rad(actJointPos[0])) + L2 * cos(deg2rad(actJointPos[0] + actJointPos[1]));
	actualTCPPos[1] = L1 * sin(deg2rad(actJointPos[0])) + L2 * sin(deg2rad(actJointPos[0] + actJointPos[1]));
	actualTCPPos[2] = actJointPos[2] + 120.0;
	actualTCPPos[3] = actJointPos[0] + actJointPos[1] + actJointPos[3];
	
	
	/*************** Return **************/
	return actualTCPPos;
	
}