#include "RoboHeader.h"


/* Inverse transformation of the Delta 2 Axes robot is calculated in this function. */
double * inverseTrfDelta2Axes (double targetPos[], struct RoboDelta2AxesParDimensionsType dim)
{
	
	/* Variables */
	double distance1, distance2;
	double basePoint[2];
	double q1, q2;
	double q1Prime, q2Prime;
	double alpha1, alpha2;
	double realPosDegree[2];
	double L1 = dim.BaseRadius;			// mm
	double L2 = dim.ArmLength;			// mm
	double L3 = dim.PipeLength;			// mm
	double L4 = dim.PlatformRadius;		// mm
	double L5 = dim.ToolHolderLength;	// mm
	double h;
	
	
	/* Calculations */
	h = sqrt(pow(L3,2.0) - pow((L1+L2-L4),2.0));
	
	basePoint[0] = 0.0;
	basePoint[1] = h + L5;
	
	distance1 = sqrt(pow((targetPos[0] - basePoint[0] + L1 - L4),2.0) + pow((targetPos[1] - basePoint[1] + L5),2.0));
	distance2 = sqrt(pow((targetPos[0] - basePoint[0] - L1 + L4),2.0) + pow((targetPos[1] - basePoint[1] + L5),2.0));
	
	q1Prime = acos((pow(distance1,2.0) + pow(L2,2.0) - pow(L3,2.0)) / (2*distance1*L2));
	q1Prime = rad2deg(q1Prime);
	
	q2Prime = acos((pow(distance2,2.0) + pow(L2,2.0) - pow(L3,2.0)) / (2*distance2*L2));
	q2Prime = rad2deg(q2Prime);
	
	alpha1 = atan((targetPos[0] - basePoint[0] + L1 - L4) / (basePoint[1] - targetPos[1] - L5));
	alpha1 = rad2deg(alpha1);
	
	alpha2 = atan((- targetPos[0] + basePoint[0] + L1 - L4) / (basePoint[1] - targetPos[1] - L5));
	alpha2 = rad2deg(alpha2);
	
	q1 = 90 - (q1Prime - alpha1);
	q2 = 90 - (q2Prime - alpha2);
	
	realPosDegree[0] = q1;
	realPosDegree[1] = q2;
	
	return realPosDegree;
	
}


/* Forward transform of the Delta 2 Axes robot is calculated in this function to find the actual position. */
double * forwardTrfDelta2Axes (double posQ[], struct RoboDelta2AxesParDimensionsType dim)
{
	
	/* Variables */
	double actualPos[2];
	double basePoint[2];
	double q1, q2;
	double L1 = dim.BaseRadius;			// mm
	double L2 = dim.ArmLength;			// mm
	double L3 = dim.PipeLength;			// mm
	double L4 = dim.PlatformRadius;		// mm
	double L5 = dim.ToolHolderLength;	// mm
	double h;
	
	/* Calculations */
	q1 = deg2rad(posQ[0]);
	q2 = deg2rad(posQ[1]);
	
	h = sqrt(pow(L3,2.0) - pow((L1+L2-L4),2.0));
	
	basePoint[0] = 0.0;
	basePoint[1] = h + L5;
	
	actualPos[0] = 0.0;
	actualPos[1] = 0.0;
	
	return actualPos;
	
}