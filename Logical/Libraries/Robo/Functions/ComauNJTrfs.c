#include "RoboHeader.h"


/* Inverse transformation of the Comau NJ 220-2.7 robot is calculated in this function. */
double * inverseTrfComauNJ220 (double targetPos[])
{
	
	/* Variables */
	double distance, maxDistance;
	double targetPosPrime[2];
	double basePoint[2];
	double alpha, beta, theta, k;
	double realPosDegree[2];
	double L1 = 1175.0;		// mm
	double L2;
	double M = 250.0;		// mm
	double N = 1355.33;		// mm
	
	/* Calculations */
	L2 = sqrt(pow(M,2.0) + pow(N,2.0));	// mm
	theta = atan(M / N);
	
	maxDistance = L1 + L2;
	
	basePoint[0] = 0.0;
	basePoint[1] = N;
	
	distance = sqrt(pow((targetPos[0] - basePoint[0]),2.0) + pow((targetPos[1] - basePoint[1]),2.0));
	
	if ((distance <= maxDistance)/* && (targetPos[1] <= dimArm.Q1_to_Q2)*/)
	{
		beta = (pow(L1,2.0) + pow(L2,2.0) - pow(distance,2.0))/(2*L1*L2);		// Cosine Theorem
		beta = acos(beta);
		beta -= M_PI / 2.0 + theta;
		targetPosPrime[0] = basePoint[0] + L1 + L2 * sin(beta + theta);
		targetPosPrime[1] = basePoint[1] - L2 * cos(beta + theta);
		beta = rad2deg(beta);
	
		k = sqrt(pow((targetPos[0] - targetPosPrime[0]),2.0) + pow((targetPos[1] - targetPosPrime[1]),2.0)) / 2.0;
		alpha = asin(k / distance);
		alpha = rad2deg(alpha);
	
		if (targetPos[0] > targetPosPrime[0])
		{
			alpha = -alpha;
		}
		
		realPosDegree[0] = 2.0 * alpha;
		realPosDegree[1] = beta - realPosDegree[0];
	}
	
	return realPosDegree;
	
}


/* Forward transform of the Comau NJ 220-2.7 robot is calculated in this function to find the actual position. */
double * forwardTrfComauNJ220 (double posQ[])
{
	
	/* Variables */
	double actualPos[2];
	double basePoint[2];
	double alpha, beta, theta;
	double L1 = 1175.0;		// mm
	double L2;
	double M = 250.0;		// mm
	double N = 1355.33;		// mm
	
	/* Calculations */
	alpha = deg2rad(posQ[0]);
	beta = deg2rad(posQ[1]);
	theta = atan(M / N);
	
	L2 = sqrt(pow(M,2.0) + pow(N,2.0));	// mm
	
	basePoint[0] = 0.0;
	basePoint[1] = N;
	
	actualPos[0] = basePoint[0] + L1 * cos(alpha) + L2 * sin(beta + theta);
	actualPos[1] = basePoint[1] - L1 * sin(alpha) - L2 * cos(beta + theta);
	
	return actualPos;
	
}


/* Inverse transformation of the Comau NJ 165-3.7 SH robot is calculated in this function. */
double * inverseTrfComauNJ165 (double targetPos[])
{
	
	/* Variables */
	double distance, distancePrime, radius, maxDistance;
	double targetPosPrimePrimeX;
	double targetPosPrimeX;
	double targetPosPrimeZ;
	double basePoint[3];
	double alpha, beta, phi, theta, k, v;
	double realPosDegree[3];
	double L1 = 850.0;		// mm
	double L2 = 1175.0;		// mm
	double L3;
	double M = 250.0;		// mm
	double N = 1674.0;		// mm
	
	/* Calculations */
	L3 = sqrt(pow(M,2.0) + pow(N,2.0));	// mm
	theta = atan(M / N);
	
	maxDistance = L1 + L2 + L3;
	
	basePoint[0] = 0.0;	//	x-axis
	basePoint[1] = 0.0;	//	y-axis
	basePoint[2] = N;	//	z-axis
	
	distance = sqrt(pow((targetPos[0] - basePoint[0]),2.0) + pow((targetPos[1] - basePoint[1]),2.0) + pow((targetPos[2] - basePoint[2]),2.0));
	
	if ((distance <= maxDistance)/* && (targetPos[1] <= dimArm.Q1_to_Q2)*/)
	{
		radius = sqrt(pow((targetPos[0] - basePoint[0]),2.0) + pow((targetPos[1] - basePoint[1]),2.0));
		targetPosPrimeX = basePoint[0] + radius;
		v = sqrt((pow((targetPos[0] - targetPosPrimeX),2.0) + pow((targetPos[1] - basePoint[1]),2.0))) / 2.0;
		phi = 2.0 * asin(v / radius);
		phi = rad2deg(phi);
		
		if (targetPos[1] > basePoint[1])
		{
			phi = -phi;
		}
		
		distancePrime = sqrt(pow((targetPosPrimeX - basePoint[0] - L1),2.0) + pow((targetPos[2] - basePoint[2]),2.0));
		beta = (pow(L2,2.0) + pow(L3,2.0) - pow(distancePrime,2.0))/(2*L2*L3);		// Cosine Theorem
		beta = acos(beta);
		beta -= M_PI / 2.0 + theta;
		targetPosPrimePrimeX = basePoint[0] + L1 + L2 + L3 * sin(beta + theta);
		targetPosPrimeZ = basePoint[2] - L3 * cos(beta + theta);
		beta = rad2deg(beta);
		
		k = sqrt(pow((targetPosPrimeX - targetPosPrimePrimeX),2.0) + pow((targetPos[2] - targetPosPrimeZ),2.0)) / 2.0;
		
		alpha = 2.0 * asin(k / distancePrime);
		alpha = rad2deg(alpha);
		
		if (targetPosPrimeX > targetPosPrimePrimeX)
		{
			alpha = -alpha;
		}
		
		realPosDegree[0] = phi; 
		realPosDegree[1] = alpha;
		realPosDegree[2] = beta - alpha;
	}
	
	return realPosDegree;
	
}


/* Forward transform of the Comau NJ 220-2.7 robot is calculated in this function to find the actual position. */
double * forwardTrfComauNJ165 (double posQ[])
{
	
	/* Variables */
	double actualPos[3];
	double basePoint[3];
	double phi, alpha, beta, theta;
	double xLength;
	double L1 = 850.0;		// mm
	double L2 = 1175.0;		// mm
	double L3;
	double M = 250.0;		// mm
	double N = 1674.0;		// mm
	
	/* Calculations */
	phi = deg2rad(posQ[0]);
	alpha = deg2rad(posQ[1]);
	beta = deg2rad(posQ[2]);
	theta = atan(M / N);
	
	L3 = sqrt(pow(M,2.0) + pow(N,2.0));	// mm
	
	basePoint[0] = 0.0;	//	x-axis
	basePoint[1] = 0.0;	//	y-axis
	basePoint[2] = N;	//	z-axis
	
	xLength = L1 + L2 * cos(alpha) + L3 * sin(beta + theta);
	
	actualPos[0] = basePoint[0] + xLength * cos(phi);
	actualPos[1] = basePoint[0] - xLength * sin(phi);
	actualPos[2] = basePoint[2] - L2 * sin(alpha) - L3 * cos(beta + theta);
	
	return actualPos;
	
}