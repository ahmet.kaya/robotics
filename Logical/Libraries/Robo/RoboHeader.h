
#ifndef _ROBO_HEADER_
#define _ROBO_HEADER_

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "Robo.h"
#include <math.h>
#include <string.h>
#ifdef __cplusplus
	};
#endif

#define FIRST_IN_STEP(var) ({					\
			if (var.Step != var.OldStep)		\
			{									\
				 var.OldStep = var.Step;		\
				 var.FirstInStep = 1;			\
			}									\
			else   								\
			{									\
				 var.FirstInStep = 0;			\
			}									\
		})										\

#define _COMAU_REBEL_Q1_POS_LIMIT +132.0
#define _COMAU_REBEL_Q1_NEG_LIMIT -132.0
#define _COMAU_REBEL_Q2_POS_LIMIT +150.0
#define _COMAU_REBEL_Q2_NEG_LIMIT -150.0
#define _COMAU_REBEL_Q3_POS_LIMIT +30.0
#define _COMAU_REBEL_Q3_NEG_LIMIT -90.0

double * inverseTrfComauNJ220 (double targetPos[]);
double * forwardTrfComauNJ220 (double posQ[]);
double * inverseTrfComauNJ165 (double targetPos[]);
double * forwardTrfComauNJ165 (double posQ[]);
double * inverseTrfDelta2Axes (double targetPos[], struct RoboDelta2AxesParDimensionsType dim);
double * forwardTrfDelta2Axes (double posQ[], struct RoboDelta2AxesParDimensionsType dim);
double * inverseTrfComauRebel (double targetTCPPos[], double actJointPos[], int type);
double * forwardTrfComauRebel (double actJointPos[], int type);
double rad2deg (double radValue);
double deg2rad (double degValue);

#endif /* _ROBO_HEADER_ */