(*===============================================================================================
            ROBO2AXES
===============================================================================================*)
(*Globally Used Types*)

TYPE
	Robo2AxesParType : 	STRUCT 
		Dimensions : Robo2AxesParDimensionsType;
		Position : ARRAY[0..1]OF LREAL; (*mm*)
		Velocity : REAL := 50.0; (*mm/sec*)
		Acceleration : REAL := 200.0; (*mm/sec^2*)
		Deceleration : REAL := 200.0; (*mm/sec^2*)
		Home : Robo2AxesParHomeType;
	END_STRUCT;
	Robo2AxesParDimensionsType : 	STRUCT 
		Q1_to_Q2 : LREAL;
		Q2_to_TCP : LREAL;
	END_STRUCT;
	Robo2AxesDiagType : 	STRUCT 
		X : MpAxisDiagExtType;
		Z : MpAxisDiagExtType;
		Q1 : MpAxisDiagExtType;
		Q2 : MpAxisDiagExtType;
	END_STRUCT;
	Robo2AxesParHomeType : 	STRUCT 
		Q1 : LREAL;
		Q2 : LREAL;
	END_STRUCT;
END_TYPE

(**)
(*Internally Used Types*)

TYPE
	Robo2AxesInternalType : 	STRUCT 
		FB : Robo2AxesInternalFBType;
		Status : Robo2AxesInternalStatusType;
	END_STRUCT;
	Robo2AxesInternalFBType : 	STRUCT 
		SetCyclicPosQ1 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ2 : MC_BR_MoveCyclicPosition;
	END_STRUCT;
	Robo2AxesInternalStatusType : 	STRUCT 
		Step : Robo2AxesStepEnum;
		OldStep : Robo2AxesStepEnum;
		FirstInStep : BOOL;
		HomeOld : BOOL;
		MoveAbsoluteOld : BOOL;
	END_STRUCT;
	Robo2AxesStepEnum : 
		(
		ROBO_2_AXES_INIT,
		ROBO_2_AXES_WAIT,
		ROBO_2_AXES_POWER,
		ROBO_2_AXES_HOME_Q1_Q2,
		ROBO_2_AXES_HOME_X_Z,
		ROBO_2_AXES_MOVE_ABS,
		ROBO_2_AXES_ERROR := 255
		);
END_TYPE

(**)
(**)
(*===============================================================================================
            ROBO3AXES
===============================================================================================*)
(*Globally Used Types*)

TYPE
	Robo3AxesParType : 	STRUCT 
		Dimensions : Robo3AxesParDimensionsType;
		Position : ARRAY[0..2]OF LREAL; (*mm*)
		Velocity : REAL := 50.0; (*mm/sec*)
		Acceleration : REAL := 200.0; (*mm/sec^2*)
		Deceleration : REAL := 200.0; (*mm/sec^2*)
		Home : Robo3AxesParHomeType;
	END_STRUCT;
	Robo3AxesParDimensionsType : 	STRUCT 
		Q1_to_Q2 : LREAL;
		Q2_to_TCP : LREAL;
	END_STRUCT;
	Robo3AxesDiagType : 	STRUCT 
		X : MpAxisDiagExtType;
		Y : MpAxisDiagExtType;
		Z : MpAxisDiagExtType;
		Q1 : MpAxisDiagExtType;
		Q2 : MpAxisDiagExtType;
		Q3 : MpAxisDiagExtType;
	END_STRUCT;
	Robo3AxesParHomeType : 	STRUCT 
		Q1 : LREAL;
		Q2 : LREAL;
		Q3 : LREAL;
	END_STRUCT;
END_TYPE

(**)
(*Internally Used Types*)

TYPE
	Robo3AxesInternalType : 	STRUCT 
		FB : Robo3AxesInternalFBType;
		Status : Robo3AxesInternalStatusType;
	END_STRUCT;
	Robo3AxesInternalFBType : 	STRUCT 
		SetCyclicPosQ1 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ2 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ3 : MC_BR_MoveCyclicPosition;
	END_STRUCT;
	Robo3AxesInternalStatusType : 	STRUCT 
		Step : Robo3AxesStepEnum;
		OldStep : Robo3AxesStepEnum;
		FirstInStep : BOOL;
		HomeOld : BOOL;
		MoveAbsoluteOld : BOOL;
	END_STRUCT;
	Robo3AxesStepEnum : 
		(
		ROBO_3_AXES_INIT,
		ROBO_3_AXES_WAIT,
		ROBO_3_AXES_POWER,
		ROBO_3_AXES_HOME_Q1_Q2_Q3,
		ROBO_3_AXES_HOME_X_Y_Z,
		ROBO_3_AXES_MOVE_ABS,
		ROBO_3_AXES_ERROR := 255
		);
END_TYPE

(**)
(**)
(*===============================================================================================
            ROBODELTA2AXES
===============================================================================================*)
(*Globally Used Types*)

TYPE
	RoboDelta2AxesParType : 	STRUCT 
		Dimensions : RoboDelta2AxesParDimensionsType;
		Position : ARRAY[0..1]OF LREAL; (*mm*)
		Velocity : REAL := 50.0; (*mm/sec*)
		Acceleration : REAL := 200.0; (*mm/sec^2*)
		Deceleration : REAL := 200.0; (*mm/sec^2*)
		Home : RoboDelta2AxesParHomeType;
	END_STRUCT;
	RoboDelta2AxesParDimensionsType : 	STRUCT 
		BaseRadius : LREAL;
		PlatformRadius : LREAL;
		ArmLength : LREAL;
		PipeLength : LREAL;
		ToolHolderLength : LREAL;
	END_STRUCT;
	RoboDelta2AxesDiagType : 	STRUCT 
		X : MpAxisDiagExtType;
		Z : MpAxisDiagExtType;
		Q1 : MpAxisDiagExtType;
		Q2 : MpAxisDiagExtType;
	END_STRUCT;
	RoboDelta2AxesParHomeType : 	STRUCT 
		Q1 : LREAL;
		Q2 : LREAL;
	END_STRUCT;
END_TYPE

(**)
(*Internally Used Types*)

TYPE
	RoboDelta2AxesInternalType : 	STRUCT 
		FB : RoboDelta2AxesInternalFBType;
		Status : RoboDelta2AxesInternalStatusType;
	END_STRUCT;
	RoboDelta2AxesInternalFBType : 	STRUCT 
		SetCyclicPosQ1 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ2 : MC_BR_MoveCyclicPosition;
	END_STRUCT;
	RoboDelta2AxesInternalStatusType : 	STRUCT 
		Step : RoboDelta2AxesStepEnum;
		OldStep : RoboDelta2AxesStepEnum;
		FirstInStep : BOOL;
		HomeOld : BOOL;
		MoveAbsoluteOld : BOOL;
	END_STRUCT;
	RoboDelta2AxesStepEnum : 
		(
		ROBO_DELTA_2_AXES_INIT,
		ROBO_DELTA_2_AXES_WAIT,
		ROBO_DELTA_2_AXES_POWER,
		ROBO_DELTA_2_AXES_HOME_Q1_Q2,
		ROBO_DELTA_2_AXES_HOME_X_Z,
		ROBO_DELTA_2_AXES_MOVE_ABS,
		ROBO_DELTA_2_AXES_ERROR := 255
		);
END_TYPE

(**)
(**)
(*===============================================================================================
            ROBOREBEL
===============================================================================================*)
(*Globally Used Types*)

TYPE
	RoboRebelParType : 	STRUCT 
		Dimensions : RoboRebelParDimensionsType;
		Position : ARRAY[0..3]OF LREAL; (*mm or degree*)
		Velocity : REAL := 50.0; (*mm/sec or degree/sec*)
		Acceleration : REAL := 200.0; (*mm/sec^2 or degree/sec^2*)
		Deceleration : REAL := 200.0; (*mm/sec^2 or degree/sec^2*)
		Home : RoboRebelParHomeType;
		Variation : RoboRebelParVariationEnum;
	END_STRUCT;
	RoboRebelParDimensionsType : 	STRUCT 
		Q1_to_Q2 : LREAL;
		Q2_to_TCP : LREAL;
	END_STRUCT;
	RoboRebelDiagType : 	STRUCT 
		X : MpAxisDiagExtType;
		Y : MpAxisDiagExtType;
		Z : MpAxisDiagExtType;
		C : MpAxisDiagExtType;
		Q1 : MpAxisDiagExtType;
		Q2 : MpAxisDiagExtType;
		Q3 : MpAxisDiagExtType;
		Q4 : MpAxisDiagExtType;
	END_STRUCT;
	RoboRebelParHomeType : 	STRUCT 
		Q1 : LREAL;
		Q2 : LREAL;
		Q3 : LREAL;
		Q4 : LREAL;
	END_STRUCT;
	RoboRebelParVariationEnum : 
		(
		REBEL_S6_045 := 45,
		REBEL_S6_060 := 60,
		REBEL_S6_075 := 75
		);
END_TYPE

(**)
(*Internally Used Types*)

TYPE
	RoboRebelInternalType : 	STRUCT 
		FB : RoboRebelInternalFBType;
		Status : RoboRebelInternalStatusType;
	END_STRUCT;
	RoboRebelInternalFBType : 	STRUCT 
		SetCyclicPosQ1 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ2 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ3 : MC_BR_MoveCyclicPosition;
		SetCyclicPosQ4 : MC_BR_MoveCyclicPosition;
	END_STRUCT;
	RoboRebelInternalStatusType : 	STRUCT 
		Step : RoboRebelStepEnum;
		OldStep : RoboRebelStepEnum;
		FirstInStep : BOOL;
		HomeOld : BOOL;
		MoveAbsoluteOld : BOOL;
	END_STRUCT;
	RoboRebelStepEnum : 
		(
		ROBO_REBEL_INIT,
		ROBO_REBEL_WAIT,
		ROBO_REBEL_POWER,
		ROBO_REBEL_HOME_Q1_Q2_Q3_Q4,
		ROBO_REBEL_HOME_X_Y_Z_C,
		ROBO_REBEL_MOVE_ABS,
		ROBO_REBEL_ERROR := 255
		);
END_TYPE
